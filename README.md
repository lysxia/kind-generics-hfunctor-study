# Generic implementation of HFunctor

This repository contains a couple of solutions to a single problem
in small(ish) generic programming in Haskell,
amounting to a case study of using [*kind-generics*][kg] and a demonstration of
general advanced patterns for programming with type classes.

[kg]: https://hackage.haskell.org/package/kind-generics

Problem: implement generic instances for the following class `HFunctor`,
which is a generalization of `Functor`:

```haskell
class HFunctor (h :: (Type -> Type) -> Type -> Type) where
  hmap :: (forall x. f x -> g x) -> h f a -> h g a

-- Also known as higher-order functors, or indexed functors.
```

The particular choice of class is actually not too important.
Most of the same challenges would be encountered with `Functor`.

## Solutions

They can be found under `src/`.

The first solution (`HFunctor1`) is based on GHC generics
(the framework for generic programming native to GHC),
which is unsatisfactory because it uses incoherent instances.

That feature can be avoided using the much more expressive
[*kind-generics*][kg] framework (`HFunctor3`).
That solution is then extended to support (a large class of) GADTs
(`HFunctor5`).

An additional generalization of the problem is possible (`HFunctor6`),
generalizing the kind of the functor `h` (with `PolyKinds`):

```haskell
class HFunctor (h :: (i -> Type) -> j -> Type) where
  hmap :: (forall x. f x -> g x) -> h f a -> h g a
```

Two other files illustrate some more technical points.

`HFunctor2` is a port of `HFunctor1` from GHC generics to *kind-generics*,
but still using incoherent instances, demonstrating that *kind-generics*
is at least as expressive as GHC generics via a fairly mechanical translation.

`HFunctor4` is a failed attempt, showing the first obstacle to generalize
`HFunctor3` to GADTs: it uses a dependently kinded type class, which you are
allowed to declare, but GHC lacks support to do anything useful with it.

### Summary

- `HFunctor0_Manual`: Non-solution to illustrate the problem, with manual
  instances.

- `HFunctor1_GHCGenericsIncoherent`: using `GHC.Generics` and
  (necessarily) incoherent instances.

- `HFunctor2_KindGenericsIncoherent`: using the *kind-generics* library
  and incoherent instances, for a close comparison with `HFunctor1`.

- `HFunctor3_KindGenericsSimpleADTs`: using the *kind-generics* library,
  and avoiding incoherent instances, but restricted to the same class
  of generic types as `HFunctor1` ("simple ADTs").

- `HFunctor4_KindGenericsGADTs_Fail`: a failed attempt to generalize `HFunctor3`
  to GADTs.

- `HFunctor5_KindGenericsGADTs`: generic `HFunctor` for a fairly big
  class of GADTs (not all of them, but you have to think harder to find
  legitimate counterexamples).

- `HFunctor6_KindGenericsGADTsPolyKinds`: generalize the kind of `HFunctor`,
  to represent indexed functors with arbitrary kinds of indices instead of
  only `Type`.
