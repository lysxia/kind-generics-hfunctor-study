{-# LANGUAGE
  DataKinds,
  FlexibleInstances,
  MultiParamTypeClasses,
  PolyKinds,
  RankNTypes,
  TypeFamilies,
  TypeOperators,
  UndecidableInstances
  #-}

-- | Failed attempt to generalize HFunctor3 to GADTs.
--
-- Too much dependent types for GHC to handle.
module HFunctor4_KindGenericsGADTs_Fail where

import Generics.Kind hiding ((:~:))
import Data.Kind (Type)
import Data.Proxy
import Data.Type.Bool
import Data.Type.Equality

type TT = Type -> Type

class HFunctor h where
  hmap :: (forall x. f x -> g x) -> h f a -> h g a

type family (++) (k :: Type) (l :: Type) :: Type where
  Type ++ l = l
  (a -> k) ++ l = a -> (k ++ l)

type family (++.) (t :: LoT k) (u :: LoT l) :: LoT (k ++ l) where
  (t :: LoT Type) ++. u = u
  t ++. u = HeadLoT t :&&: (TailLoT t ++. u)

-- Here k must be an explicit argument to avoid the ambiguity
class GHFunctor k (gh :: LoT (k ++ (TT -> Type -> Type)) -> Type) where
  ghmap ::
   forall
     (r :: LoT k)
     (f :: Type -> Type)
     (g :: Type -> Type)
     (a :: Type)
   .
   Proxy '(r, a) -> 
   (forall x. f x -> g x) ->
   gh (r ++. LoT2 f a) -> gh (r ++. LoT2 g a)

{-
-- Error:
-- Illegal type synonym application

instance GHFunctor k gh => GHFunctor k (M1 i c gh) where
  ghmap p f (M1 u) = M1 (ghmap p f u)
-}
