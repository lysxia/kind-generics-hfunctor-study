{-# LANGUAGE
  AllowAmbiguousTypes,
  ConstraintKinds,
  DataKinds,
  DeriveGeneric,
  FlexibleContexts,
  FlexibleInstances,
  KindSignatures,
  MultiParamTypeClasses,
  PolyKinds,
  RankNTypes,
  ScopedTypeVariables,
  TypeApplications,
  TypeFamilies,
  TypeOperators,
  TemplateHaskell,
  QuantifiedConstraints,
  UndecidableInstances,
  UndecidableSuperClasses
  #-}

-- | With instances derived using kind-generics
module HFunctor3_KindGenericsSimpleADTs where

import Data.Kind (Type)
import Data.Type.Bool
import Data.Type.Equality

import Generics.Kind hiding ((:~:))
import Generics.Kind.TH

type TT = Type -> Type

class HFunctor (h :: TT -> TT) where
  hmap :: (forall a. f a -> g a) -> h f a -> h g a

--

genericHmap ::
  ( GenericK h, GenericK h 
  , GHFunctor (RepK h)
  ) =>
  (forall x. f x -> g x) ->
  h f a -> h g a
genericHmap f = toK . ghmap f . fromK

class GHFunctor hf where
  ghmap :: (forall x. f x -> g x) -> hf (LoT2 f a) -> hf (LoT2 g a)

instance GHFunctor hf => GHFunctor (M1 i c hf) where
  ghmap n (M1 u) = M1 (ghmap n u)

instance
  ( GHFunctor hf1, GHFunctor hf2
  ) => GHFunctor (hf1 :+: hf2) where
  ghmap n (L1 u) = L1 (ghmap n u)
  ghmap n (R1 v) = R1 (ghmap n v)

instance
  ( GHFunctor hf1, GHFunctor hf2
  ) => GHFunctor (hf1 :*: hf2) where
  ghmap n (u :*: v) = ghmap n u :*: ghmap n v

instance GHFunctor_Field' t => GHFunctor (Field t) where
  ghmap n (Field u :: Field t (LoT2 f a))
    = Field (ghmap_field @t (Proxy @a) n u)

class    GHFunctor_Field (FieldCaseOf t) t => GHFunctor_Field' t
instance GHFunctor_Field (FieldCaseOf t) t => GHFunctor_Field' t

ghmap_field :: forall t a f g. GHFunctor_Field' t =>
  Proxy a ->
  (forall x. f x -> g x) ->
  Interpret t (LoT2 f a) -> Interpret t (LoT2 g a)
ghmap_field = ghmap_field_ @(FieldCaseOf t) @t

-- fieldcase ~ FieldCaseOf t
class GHFunctor_Field fieldcase t where
  ghmap_field_ ::
    Proxy a ->
    (forall x. f x -> g x) ->
    Interpret t (LoT2 f a) -> Interpret t (LoT2 g a)

instance IsConstClass t => GHFunctor_Field 'CONST t where
  ghmap_field_ a (n :: forall x. f x -> g x) = eqfun (ghmap_CONST @t a n)

ghmap_CONST ::
  forall t f g a.
  IsConstClass t =>
  Proxy a ->
  (forall x. f x -> g x) ->
  Interpret t (LoT2 f a) :~: Interpret t (LoT2 g a)
ghmap_CONST _ _
  | Refl <- isConst @t f a
  , Refl <- isConst @t g a
  = Refl
  where
    f = Proxy @f
    g = Proxy @g
    a = Proxy @a

eqfun :: (a :~: b) -> a -> b
eqfun Refl = id

-- (forall f g a. Interpret t (LoT2 f a) ~ Interpret t (LoT2 g a))
class IsConstClass t where
  isConst ::
    Proxy f ->
    Proxy a ->
    Interpret t (LoT2 f a) :~: Interpret (NoOccurZ t) (LoT1 a)

instance IsConstClass_Var v => IsConstClass ('Var v) where
  isConst = isConst_Var @v

class IsConstClass_Var v where
  isConst_Var ::
    Proxy f ->
    Proxy a ->
    InterpretVar v (LoT2 f a) :~: InterpretVar (NoOccurZVar v) (LoT1 a)

instance IsConstClass_Var ('VS v) where
  isConst_Var _ _ = Refl

instance IsConstClass ('Kon k) where
  isConst _ _ = Refl

instance (IsConstClass t, IsConstClass u) => IsConstClass (t ':@: u) where
  isConst f a = eqap (isConst @t f a) (isConst @u f a)

eqap :: forall b1 b2 c1 c2. (b1 :~: b2) -> (c1 :~: c2) -> (b1 c1 :~: b2 c2)
eqap Refl Refl = Refl

instance IsConstClass t => GHFunctor_Field 'F ('Var 'VZ ':@: t) where
  ghmap_field_ a (n :: forall x. f x -> g x) =
    case ghmap_CONST @t a n of
      Refl -> n

instance
  ( IsConstClass h
  , IsConstClass t
  , forall a. Class_Interpret_NoOccurZ HFunctor h (LoT1 a)
  ) => GHFunctor_Field 'H (h ':@: 'Var 'VZ ':@: t) where
  ghmap_field_ (a :: Proxy a) (n :: forall x. f x -> g x)
    | Refl <- isConst @h f a
    , Refl <- isConst @h g a
    , Refl <- ghmap_CONST @t a n
    = generalize @(Class_Interpret_NoOccurZ HFunctor h (LoT1 a))
        (hmap @(Interpret (NoOccurZ h) (LoT1 a)) n)
    where
      f = Proxy @f
      g = Proxy @g

class    c (Interpret (NoOccurZ h) l) => Class_Interpret_NoOccurZ c h l
instance c (Interpret (NoOccurZ h) l) => Class_Interpret_NoOccurZ c h l

generalize :: forall c r. (c => r) -> (c => r)
generalize x = x

instance
  ( IsConstClass p
  , forall a. Class_Interpret_NoOccurZ Functor p (LoT1 a)
  , GHFunctor_Field' t
  ) => GHFunctor_Field 'P (p ':@: t) where
  ghmap_field_ (a :: Proxy a) (n :: forall x. f x -> g x)
    | Refl <- isConst @p f a
    , Refl <- isConst @p g a
    = generalize @(Class_Interpret_NoOccurZ Functor p (LoT1 a))
        (fmap (ghmap_field @t a n))
    where
      f = Proxy @f
      g = Proxy @g

-- Let t be a term which may contain two variables vf and va
data FieldCase
  = CONST  -- The term t contains no occurrence of vf
  | F      -- The term t is of the form (vf u) (where u should contain no occurence of vf)
  | H      -- The term t is of the form (h vf u) (where h and u should contain no occurence of vf)
  | P      -- The term t is of the form (p u) (where p should contain no occurence of vf, and is a functor)
  | FAIL   -- Unsupported case

-- This represents the list '[TT, Type], using (->) as cons, Type as nil.
type MyArity = TT -> Type -> Type

type FieldCaseOf (t :: Atom MyArity k)
  = '(IsConst t, 'CONST) ||? '(IsF t, 'F) ||? '(IsH t, 'H) ||? '(IsP t, 'P) ||? 'FAIL

infixr 1 ||?

type family (||?) (x :: (Bool, FieldCase)) (y :: FieldCase) :: FieldCase where
  '( 'True , c) ||? _ = c
  '( 'False, _) ||? d = d

type family IsConst (t :: Atom MyArity k) :: Bool where
  IsConst ('Var v) = IsNotZ v
  IsConst ('Kon _) = 'True
  IsConst (f ':@: g) = IsConst f && IsConst g
  -- Other constructors unsupported (only simple ADTs)

type family IsNotZ (v :: TyVar MyArity k) :: Bool where
  IsNotZ 'VZ = 'False
  IsNotZ ('VS _) = 'True

type family IsF (t :: Atom MyArity k) :: Bool where
  IsF ( 'Var 'VZ ':@: _ ) = 'True
  IsF _ = 'False

type family IsH (t :: Atom MyArity k) :: Bool where
  IsH ( _ ':@: 'Var 'VZ ':@: _ ) = 'True
  IsH _ = 'False

type family IsP (t :: Atom MyArity k) :: Bool where
  IsP ( _ ':@: _) = 'True
  IsP _ = 'False

type family NoOccurZ (t :: Atom (k -> r) j) :: Atom r j where
  NoOccurZ ('Var v) = 'Var (NoOccurZVar v)
  NoOccurZ ('Kon k) = 'Kon k
  NoOccurZ (t ':@: u) = NoOccurZ t ':@: NoOccurZ u

type family NoOccurZVar (t :: TyVar (k -> r) j) :: TyVar r j where
  NoOccurZVar ('VS v) = v

---

data ABC f a = ABC (f Int) (f Bool) (f ())
$(deriveGenericK ''ABC)

instance HFunctor ABC where
  hmap = genericHmap

---

data FreeF b f a = Pure b | Free (f a)
$(deriveGenericK ''FreeF)

instance HFunctor (FreeF b) where
  hmap = genericHmap

data CoFreeF b f a = b :< f a
$(deriveGenericK ''CoFreeF)

instance HFunctor (CoFreeF b) where
  hmap = genericHmap

--

newtype ReaderT r f a = ReaderT (r -> f a)
$(deriveGenericK ''ReaderT)

instance HFunctor (ReaderT r) where
  hmap = genericHmap

--

data HSum (h1 :: TT -> TT) (h2 :: TT -> TT) (f :: TT) a
  = HL (h1 f a) | HR (h2 f a)
$(deriveGenericK ''HSum)

instance (HFunctor h1, HFunctor h2) => HFunctor (HSum h1 h2) where
  hmap = genericHmap
