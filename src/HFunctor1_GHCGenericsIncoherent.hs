{-# LANGUAGE
                          -- WHERE EACH EXTENSION IS NEEDED

  -- The definition of HFunctor
  RankNTypes,             -- Part of HFunctor definition
  KindSignatures,         -- Annotating the kind of HFunctor (cosmetic, unless PolyKinds is on)
  DataKinds,              -- Using type synonym in KindSignatures

  -- The generic implementation of HFunctor
  MultiParamTypeClasses,  -- GHFunctor
  FlexibleContexts,       -- Always with MultiParamTypeClasses or TypeFamilies (implicitly used in Generic's Rep)
  FlexibleInstances,      -- Always with MultiParamTypeClasses
  TypeOperators,          -- (:*:), (:+:)
  EmptyCase,              -- V1 instance
  ConstraintKinds,        -- Constraint synonyms for prettier UX (cosmetic)
  DeriveGeneric,          -- Users/examples must derive Generic

  -- DerivingVia
  DerivingVia,            -- Duh.
  QuantifiedConstraints,  -- In the context of the Generically instance
  ScopedTypeVariables,    -- In the body of the Generically instance
  AllowAmbiguousTypes,    -- The generalize function is ambiguous (cosmetic)
  TypeApplications,       -- For using generalize
  UndecidableInstances    -- Part of a trick for using QuantifiedConstraints with TypeFamilies
  #-}

-- | With instances derived using GHC.Generics (+ INCOHERENT instances)
module HFunctor1_GHCGenericsIncoherent where

import Data.Kind (Type)
import GHC.Generics

-- * The HFunctor class

type TT = Type -> Type

class HFunctor (h :: TT -> TT) where
  hmap :: (forall a. f a -> g a) -> h f a -> h g a

-- * Examples
--
-- Test cases

data ABC f a = ABC (f Int) (f Bool) (f ())
  deriving Generic

instance HFunctor ABC where
  hmap = genericHmap

--

data FreeF b f a = Pure b | Free (f a)
  deriving Generic

instance HFunctor (FreeF b) where
  hmap = genericHmap

--

data CoFreeF b f a = b :< f a
  deriving Generic

instance HFunctor (CoFreeF b) where
  hmap = genericHmap

--

newtype ReaderT r f a = ReaderT (r -> f a)
  deriving Generic

instance HFunctor (ReaderT r) where
  hmap = genericHmap

--

data HSum (h1 :: TT -> TT) (h2 :: TT -> TT) (f :: TT) a
  = HL (h1 f a) | HR (h2 f a)
  deriving Generic

instance (HFunctor h1, HFunctor h2) => HFunctor (HSum h1 h2) where
  hmap = genericHmap

-- * Generic HFunctor

-- | Generic implementation of hmap
genericHmap ::
  GenericHFunctor_ h f g a =>
  (forall x. f x -> g x) ->
  h f a -> h g a
genericHmap f = to . ghmap f . from

-- | Constraint for genericHmap
type GenericHFunctor_ h f g a =
  ( Generic (h f a)
  , Generic (h g a)
  , GHFunctor f g (Rep (h f a)) (Rep (h g a))
  )

-- | The GHFunctor class
class GHFunctor f g hf hg where
  ghmap :: (forall x. f x -> g x) -> hf p -> hg p

-- * GHFunctor implementation

-- ** Instances for M1, (:+:), (:*:), U1, V1

instance GHFunctor f g hf hg => GHFunctor f g (M1 i c hf) (M1 i c hg) where
  ghmap n (M1 u) = M1 (ghmap n u)

instance
  ( GHFunctor f g hf1 hg1, GHFunctor f g hf2 hg2
  ) => GHFunctor f g (hf1 :+: hf2) (hg1 :+: hg2) where
  ghmap n (L1 u) = L1 (ghmap n u)
  ghmap n (R1 v) = R1 (ghmap n v)

instance
  ( GHFunctor f g hf1 hg1, GHFunctor f g hf2 hg2
  ) => GHFunctor f g (hf1 :*: hf2) (hg1 :*: hg2) where
  ghmap n (u :*: v) = ghmap n u :*: ghmap n v

instance GHFunctor f g V1 V1 where
  ghmap _ v = case v of {}

instance GHFunctor f g U1 U1 where
  ghmap _ _ = U1

-- ** Instance for K1 (fields)

-- | Defers to GHFunctor_Field
instance GHFunctor_Field f g a b => GHFunctor f g (K1 i a) (K1 i b) where
  ghmap n (K1 u) = K1 (ghmap_field n u)

-- *** The GHFunctor_Field class
class GHFunctor_Field f g a b where
  ghmap_field :: (forall x. f x -> g x) -> a -> b

-- *** Instances

-- | Field of a constant type.
--
-- Used by FreeF and CoFreeF
instance {-# INCOHERENT #-} GHFunctor_Field f g a a where
  ghmap_field _ = id

-- | Field which is of the form (f y), so we can apply (n :: forall x. f x -> g x).
--
-- Used by all of them
instance {-# INCOHERENT #-} GHFunctor_Field f g (f y) (g y) where
  ghmap_field n = n

-- | Field which uses (->).
--
-- Used by ReaderT
instance {-# INCOHERENT #-}
  ( GHFunctor_Field f g t s
  , GHFunctor_Field f g a b
  ) => GHFunctor_Field f g (s -> a) (t -> b) where
  ghmap_field n u = ghmap_field n . u . ghmap_field n

-- | Field which uses another HFunctor.
--
-- Used by HSum
instance {-# INCOHERENT #-}
  ( HFunctor h
  ) => GHFunctor_Field f g (h f a) (h g a) where
  ghmap_field = hmap

-- * DerivingVia

-- ** Internal machinery

newtype Generically (h :: TT -> TT) f a = Generically (h f a)

class    GenericHFunctor_ h f g a => GenericHFunctor h f g a
instance GenericHFunctor_ h f g a => GenericHFunctor h f g a

generalize :: forall c r. (c => r) -> (c => r)
generalize x = x

instance (forall f g a. GenericHFunctor h f g a) => HFunctor (Generically h) where
  hmap (n :: forall x. f x -> g x) (Generically (u :: h f a))
    = Generically
        (generalize @(GenericHFunctor h f g a)
          (genericHmap n u))

-- ** DerivingVia example

-- A test case

data Record (f :: TT) a = R (f Int) (f Bool) (f ())
  deriving Generic
  deriving HFunctor via (Generically Record)
