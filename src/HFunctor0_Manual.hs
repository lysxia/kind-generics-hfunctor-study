{-# LANGUAGE
  DataKinds,
  KindSignatures,
  RankNTypes
  #-}

-- | With handwritten instances.
module HFunctor0_Manual where

import Data.Kind (Type)

type TT = Type -> Type

class HFunctor (h :: TT -> TT) where
  hmap :: (forall a. f a -> g a) -> h f a -> h g a

---

data ABC f a = ABC (f Int) (f Bool) (f ())

instance HFunctor ABC where
  hmap n (ABC a b c) = ABC (n a) (n b) (n c)

---

data FreeF b f a = Pure b | Free (f a)

instance HFunctor (FreeF b) where
  hmap n (Pure y) = Pure y
  hmap n (Free x) = Free (n x)

data CoFreeF b f a = b :< f a

instance HFunctor (CoFreeF b) where
  hmap n (y :< x) = y :< n x

--

-- Pop quiz:
-- Are Free and CoFree instances of HFunctor?

data Free f b = Pure' b | Free' (f (Free f b))
data CoFree f b = b :<. f (CoFree f b)

--

newtype ReaderT r f a = ReaderT (r -> f a)

instance HFunctor (ReaderT r) where
  hmap n (ReaderT u) = ReaderT (n . u)
