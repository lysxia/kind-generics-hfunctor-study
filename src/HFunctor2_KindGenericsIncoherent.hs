{-# LANGUAGE
  DataKinds,
  DeriveGeneric,
  FlexibleContexts,
  FlexibleInstances,
  KindSignatures,
  MultiParamTypeClasses,
  RankNTypes,
  TypeFamilies,
  TypeOperators,
  TemplateHaskell,
  UndecidableInstances
  #-}

-- | With instances derived using kind-generics (+ INCOHERENT instances)
module HFunctor2_KindGenericsIncoherent where

import Data.Kind (Type)

import Generics.Kind
import Generics.Kind.TH

type TT = Type -> Type

class HFunctor (h :: TT -> TT) where
  hmap :: (forall a. f a -> g a) -> h f a -> h g a

--

genericHmap ::
  ( GenericK (h f a), GenericK (h g a)
  , GHFunctor f g (RepK (h f a)) (RepK (h g a))
  ) =>
  (forall x. f x -> g x) ->
  h f a -> h g a
genericHmap f = toK . ghmap f . fromK

class GHFunctor f g hf hg where
  ghmap :: (forall x. f x -> g x) -> hf LoT0 -> hg LoT0

instance GHFunctor f g hf hg => GHFunctor f g (M1 i c hf) (M1 i c hg) where
  ghmap n (M1 u) = M1 (ghmap n u)

instance
  ( GHFunctor f g hf1 hg1, GHFunctor f g hf2 hg2
  ) => GHFunctor f g (hf1 :+: hf2) (hg1 :+: hg2) where
  ghmap n (L1 u) = L1 (ghmap n u)
  ghmap n (R1 v) = R1 (ghmap n v)

instance
  ( GHFunctor f g hf1 hg1, GHFunctor f g hf2 hg2
  ) => GHFunctor f g (hf1 :*: hf2) (hg1 :*: hg2) where
  ghmap n (u :*: v) = ghmap n u :*: ghmap n v

instance
  ( GHFunctor_Field f g (Interpret t LoT0) (Interpret u LoT0)
  ) => GHFunctor f g (Field t) (Field u) where
  ghmap n (Field u) = Field (ghmap_field n u)

class GHFunctor_Field f g a b where
  ghmap_field :: (forall x. f x -> g x) -> a -> b

-- Used by FreeF and CoFreeF
instance {-# INCOHERENT #-} GHFunctor_Field f g a a where
  ghmap_field _ = id

instance {-# INCOHERENT #-} GHFunctor_Field f g (f a) (g a) where
  ghmap_field n = n

-- Used by ReaderT
instance {-# INCOHERENT #-}
  ( GHFunctor_Field f g t s
  , GHFunctor_Field f g a b
  ) => GHFunctor_Field f g (s -> a) (t -> b) where
  ghmap_field n u = ghmap_field n . u . ghmap_field n

-- Used by HSum
instance {-# INCOHERENT #-}
  ( HFunctor h
  ) => GHFunctor_Field f g (h f a) (h g a) where
  ghmap_field = hmap

---

data ABC f a = ABC (f Int) (f Bool) (f ())
$(deriveGenericK ''ABC)

instance HFunctor ABC where
  hmap = genericHmap

---

data FreeF b f a = Pure b | Free (f a)
$(deriveGenericK ''FreeF)

instance HFunctor (FreeF b) where
  hmap = genericHmap

data CoFreeF b f a = b :< f a
$(deriveGenericK ''CoFreeF)

instance HFunctor (CoFreeF b) where
  hmap = genericHmap

--

newtype ReaderT r f a = ReaderT (r -> f a)
$(deriveGenericK ''ReaderT)

instance HFunctor (ReaderT r) where
  hmap = genericHmap

--

data HSum (h1 :: TT -> TT) (h2 :: TT -> TT) (f :: TT) a
  = HL (h1 f a) | HR (h2 f a)
$(deriveGenericK ''HSum)

instance (HFunctor h1, HFunctor h2) => HFunctor (HSum h1 h2) where
  hmap = genericHmap
