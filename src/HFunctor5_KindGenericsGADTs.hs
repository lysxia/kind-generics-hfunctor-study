{-# LANGUAGE
  ConstraintKinds,
  DataKinds,
  GADTs,
  InstanceSigs,
  PolyKinds,
  AllowAmbiguousTypes,
  EmptyCase,
  FlexibleContexts,
  FlexibleInstances,
  QuantifiedConstraints,
  MultiParamTypeClasses,
  UndecidableSuperClasses,
  TypeApplications,
  TypeOperators,
  TypeFamilies,
  TemplateHaskell,
  UndecidableInstances,
  ScopedTypeVariables,
  RankNTypes,
  NoStarIsType #-}

-- | With instanced derived using kind-generics, supporting GADTs
module HFunctor5_KindGenericsGADTs where

import Generics.Kind hiding ((:~:))
import Generics.Kind.TH
import Data.Kind (Constraint, Type)
import Data.Proxy
import Data.Type.Bool
import Data.Type.Equality

-- * The HFunctor class

type TT = Type -> Type

class HFunctor (h :: TT -> Type -> Type) where
  hmap :: (forall x. f x -> g x) -> h f a -> h g a

-- * Generic HFunctor

-- ** Type-level lists

-- Append homogeneous lists
type family (++) (k :: Type) (l :: Type) :: Type where
  Type ++ l = l
  (a -> k) ++ l = a -> (k ++ l)

-- Append heterogeneous lists
type family (++.) (t :: LoT k) (u :: LoT l) :: LoT kl where
  (nil :: LoT Type) ++. u = u
  t ++. u = HeadLoT t :&&: (TailLoT t ++. u)

-- Note on (++.)
-- kl should be (k ++ l), but that causes GHFunctor to use a type family in its
-- kind which makes it impossible to define nontrivial instances
-- (e.g., M1, see also file Functor4).

{-
-- The definition of (++.) we really want

-- Append heterogeneous lists
type family (++.) (t :: LoT k) (u :: LoT l) :: LoT (k ++ l) where
  (t :: LoT Type) ++. u = u
  t ++. u = HeadLoT t :&&: (TailLoT t ++. u)
-}

-- ** The GHFunctor class

-- Generic HFunctor: class of generic representations of types for which
-- we can implement HFunctor generically.
--
-- Invariant:
--   kw ~ (k ++ (TT -> Type -> Type))
--     (kw is a list of types whose last two elements are TT and Type)
class GHFunctor k kw (gh :: LoT kw -> Type) where
  ghmap ::
    forall (r :: LoT k) (f :: TT) (g :: TT) (a :: Type).
    (Proxy r, Proxy a) ->
    (forall x. f x -> g x) ->
    gh (r ++. LoT2 f a) -> gh (r ++. LoT2 g a)

-- Note on GHFunctor
-- For type classes, we use a 'Proxy' for every ambiguous type variable that is
-- quantified by the method itself, so it is not part of the class declaration
-- itself (between the keywords @class@ and @where@),
--
-- It is definitely possible to not use 'Proxy' arguments and do visible type
-- applications, but this requires duplicating the type signature in every
-- method to put the type variables of each method in scope.
-- That becomes really verbose quickly.
--
-- We bundle all the proxies in a tuple so they can be passed around easily
-- when they don't change.

-- *** Wrappers for users

-- Wrapper fixing kw
type GHFunctor'_ k (gh :: LoT (k ++ (TT -> Type -> Type)) -> Type)
  = GHFunctor k (k ++ (TT -> Type -> Type)) gh

-- Wrapper for the base case where k = Type.
class    GHFunctor'_ Type gh => GHFunctor' (gh :: LoT (TT -> Type -> Type) -> Type)
instance GHFunctor'_ Type gh => GHFunctor' (gh :: LoT (TT -> Type -> Type) -> Type)

-- Users only see this one function

genericHmap ::
  forall (h :: TT -> TT) (f :: TT) (g :: TT) (a :: Type).
  (GenericK h, GHFunctor' (RepK h)) =>
  (forall x. f x -> g x) ->
  h f a -> h g a
genericHmap n = toK . ghmap @Type @(TT -> Type -> Type) (Proxy, Proxy) n . fromK

-- ** Implementing GHFunctor

-- *** Meta, sums, products, and their units
--
-- GHFunctor instances for M1, (:+:), (:*:), V1, U1

instance (GHFunctor k kw gh) => GHFunctor k kw (M1 _x _y gh) where
  ghmap p n (M1 u) = M1 (ghmap p n u)

instance (GHFunctor k kw gh1, GHFunctor k kw gh2) => GHFunctor k kw (gh1 :+: gh2) where
  ghmap p n (L1 u) = L1 (ghmap p n u)
  ghmap p n (R1 v) = R1 (ghmap p n v)

instance (GHFunctor k kw gh1, GHFunctor k kw gh2) => GHFunctor k kw (gh1 :*: gh2) where
  ghmap p n (u :*: v) = ghmap p n u :*: ghmap p n v

instance GHFunctor k kw U1 where
  ghmap p n _ = U1

instance GHFunctor k kw V1 where
  ghmap p n v = case v of {}

-- *** Existentials (Exists)

-- GHFunctor instance for Exists
--
-- Something odd is going on here that I don't understand.
-- In the type signature of the Exists pattern,
-- the @(r ++. LoT2 f a)@ should be redundant, but if we just replace it with a
-- fresh variable @r'@, it forgets that it is actually equal to (r ++. LoT2 f a)...
instance GHFunctor (t -> k) (t -> kw) gh => GHFunctor k kw (Exists t gh) where
  ghmap
    (_ :: Proxy r, a :: Proxy a)
    (n :: forall x. f x -> g x)
    (Exists (u :: gh (z ':&&: (r ++. LoT2 f a))))
    = Exists (ghmap @(t -> k) @(t -> kw) (zr, a) n u)
    where
      zr = Proxy @(z ':&&: r)

-- *** Constraints (:=>:)

-- Some machinery from the constraints library

data Dict c where
  Dict :: c => Dict c

data c :- d where
  Sub :: (c => Dict d) -> (c :- d)

subRefl :: (c :~: d) -> (c :- d)
subRefl Refl = Sub Dict

-- GHFunctor instance for (:=>:)
instance
  ( IsConstClass k kw c
  , GHFunctor k kw t
  ) => GHFunctor k kw (c :=>: t) where
  ghmap p n (SuchThat x) =
    case subRefl (ghmap_CONST @k @kw @Constraint @c p n) of
      Sub Dict -> SuchThat (ghmap @k @kw p n x)

-- *** Fields

-- GHFunctor instance for Field
--
-- Defers to another class GHFunctor_Field which involves a lot of complexity.
instance GHFunctor_Field' k kw t => GHFunctor k kw (Field t) where
  ghmap p n (Field u) = Field (ghmap_Field' @k @kw @t p n u) where

-- ** Handling fields

-- The GHFunctor_Field class: handling fields of constructors.
--
-- We use the fieldcase index to pick between different implementation
-- strategies suitable for different field types.
--
-- Invariant:
--   fieldcase ~ FieldCaseOF t
--
-- We don't codify that explicitly as a superclass constraint
-- because this causes instances to be annoyingly verbose.
class GHFunctor_Field k kw fieldcase (t :: Atom kw Type) where
  ghmap_Field ::
    forall (r :: LoT k) (f :: TT) (g :: TT) (a :: Type).
    (Proxy r, Proxy a) ->
    (forall x. f x -> g x) ->
    Interpret t (r ++. LoT2 f a) -> Interpret t (r ++. LoT2 g a)

-- *** Wrapper for users of the GHFunctor_Field class

-- Users; the GHFunctor instance for Field (above), and the GHFunctor_Field instance for the P case (below).

class    GHFunctor_Field k kw (FieldCaseOf t) t => GHFunctor_Field' k kw (t :: Atom kw Type)
instance GHFunctor_Field k kw (FieldCaseOf t) t => GHFunctor_Field' k kw (t :: Atom kw Type)

ghmap_Field' ::
  forall k kw (t :: Atom kw Type) (r :: LoT k) (f :: TT) (g :: TT) (a :: Type).
  GHFunctor_Field' k kw t =>
  (Proxy r, Proxy a) ->
  (forall x. f x -> g x) ->
  Interpret t (r ++. LoT2 f a) -> Interpret t (r ++. LoT2 g a)
ghmap_Field' = ghmap_Field @k @kw @(FieldCaseOf t) @t

-- *** Identifying field cases

-- Let t be a term in a context (r ++. LoT2 vf va) (in particular, it may contain two variables vf and va)
data FieldCase
  = CONST  -- The term t contains no occurrence of vf
  | F      -- The term t is of the form (vf u) (where u should contain no occurence of vf)
  | H      -- The term t is of the form (h vf u) (where h and u should contain no occurence of vf)
  | P      -- The term t is of the form (p u) (where p should contain no occurence of vf, and is a functor)
  | FAIL   -- Unsupported case (for the last branch of FieldCaseOf; we could also use a stuck type family instead)

type FieldCaseOf (t :: Atom d Type)
  = '(IsConst t, 'CONST) ||? '(IsF t, 'F) ||? '(IsH t, 'H) ||? '(IsP t, 'P) ||? 'FAIL

infixr 1 ||?

type family (||?) (x :: (Bool, FieldCase)) (y :: FieldCase) :: FieldCase where
  '( 'True , c) ||? _ = c
  '( 'False, _) ||? d = d

type family IsConst (t :: Atom d k) :: Bool where
  IsConst ('Var v) = Not (IsSecondToLast v)
  IsConst ('Kon _) = 'True 
  IsConst (f ':@: g) = IsConst f && IsConst g
  IsConst (f ':&: g) = IsConst f && IsConst g
  IsConst (ForAll f) = IsConst f
  IsConst (f ':=>>: g) = IsConst f && IsConst g

type family IsSecondToLast (v :: TyVar d k) :: Bool where
  IsSecondToLast ('VZ :: TyVar (TT -> Type -> Type) TT) = 'True
  IsSecondToLast 'VZ = 'False
  IsSecondToLast ('VS v) = IsSecondToLast v

type family IsF (t :: Atom d k) :: Bool where
  IsF ( 'Var v ':@: _ ) = IsSecondToLast v
  IsF _ = 'False

type family IsH (t :: Atom d k) :: Bool where
  IsH ( _ ':@: 'Var v ':@: _ ) = IsSecondToLast v
  IsH _ = 'False

type family IsP (t :: Atom d k) :: Bool where
  IsP (w ':@: _) = IsConst w
  IsP _ = 'False

-- *** Instances for each case

-- **** CONST Fields

instance IsConstClass k kw t => GHFunctor_Field k kw 'CONST t where
  ghmap_Field p n = eqfun (ghmap_CONST @k @kw @Type @t p n)

eqfun :: (a :~: b) -> (a -> b)
eqfun Refl = id

ghmap_CONST ::
  forall k kw o (t :: Atom kw o) (r :: LoT k) (f :: TT) (g :: TT) (a :: Type).
  IsConstClass k kw t =>
  (Proxy r, Proxy a) ->
  (forall x. f x -> g x) ->
  Interpret t (r ++. LoT2 f a) :~: Interpret t (r ++. LoT2 g a)
ghmap_CONST (r, a) n
  | Refl <- isConst @k @kw @t (r, f, a)
  , Refl <- isConst @k @kw @t (r, g, a)
  = Refl
  where
    f = Proxy @f
    g = Proxy @g

class IsConstClass k kw (t :: Atom kw o) where
  isConst ::
    forall (r :: LoT k) (f :: TT) (a :: Type).
    (Proxy r, Proxy f, Proxy a) ->
    Interpret t (r ++. LoT2 f a) :~: Interpret (NoOccurN2 t) (r ++. LoT1 a :: LoT (k ++ TT))

instance IsConstClassVar k kw v => IsConstClass k kw ('Var v) where
  isConst = isConstVar @k @kw @v

instance IsConstClass k kw ('Kon c) where
  isConst _ = Refl

instance (IsConstClass k kw f, IsConstClass k kw g) => IsConstClass k kw (f ':@: g) where
  isConst p = eqap (isConst @k @kw @f p) (isConst @k @kw @g p)

eqap :: forall a1 b1 a2 b2. (a1 :~: b1) -> (a2 :~: b2) -> (a1 a2 :~: b1 b2)
eqap Refl Refl = Refl

-- Note: what about ForAll and (:&:)?
--
-- instance IsConstClass k kw f => IsConstClass k kw (ForAll f) where
-- instance IsConstClass k kw (f ':&: g) where
--
-- ^ Not possible.
-- (:~:) could be replaced with (->) in isConst, but then ghmap_Field
-- can no longer be defined as @id@.

class IsConstClassVar k kw (v :: TyVar kw o) where
  isConstVar ::
    forall (r :: LoT k) (f :: TT) (a :: Type).
    (Proxy r, Proxy f, Proxy a) ->
    InterpretVar v (r ++. LoT2 f a) :~: InterpretVar (NoOccurN2Var v) (r ++. LoT1 a :: LoT (k ++ TT))

instance IsConstClassVar Type (TT -> Type -> Type) ('VS 'VZ) where
  isConstVar _ = Refl

instance IsConstClassVar (y -> k) (y -> kw) 'VZ where
  isConstVar _ = Refl

instance
  ( IsConstClassVar k kw v
  , kw ~ (y1 -> y2 -> k')  -- Refine the kind of v so (NoOccurN2Var ('VS v)) reduces
  ) => IsConstClassVar (y -> k) (y -> kw) ('VS v) where
  isConstVar (_ :: Proxy r, f, a) = isConstVar @k @kw @v (tl, f, a) where
    tl = Proxy @(TailLoT r)

-- Refresh a term which does not contain occurences of the second-to-last
-- DeBruijn index, removing that index from the term's context.
--
-- Invariants:
--   kw ~ (k ++ (TT -> Type -> Type))
--   kv ~ (k ++       (Type -> Type))
type family NoOccurN2 (t :: Atom kw o) :: Atom kv o where
  NoOccurN2 ('Var v) = 'Var (NoOccurN2Var v)
  NoOccurN2 ('Kon c) = 'Kon c
  NoOccurN2 (t ':@: u) = NoOccurN2 t ':@: NoOccurN2 u

type family NoOccurN2Var (v :: TyVar kw o) :: TyVar kv o where
  -- Drop second-to-last variable
  NoOccurN2Var ('VS 'VZ :: TyVar (_ -> Type -> Type) Type) = ('VZ :: TyVar TT Type)
  NoOccurN2Var 'VZ = 'VZ
  NoOccurN2Var ('VS v) = 'VS (NoOccurN2Var v)

-- **** F Fields

instance (IsFClass k kw v, IsConstClass k kw t) => GHFunctor_Field k kw 'F ('Var v ':@: t) where
  ghmap_Field p (n :: forall x. f x -> g x)
    | Refl <- isF @k @kw @v @f p
    , Refl <- isF @k @kw @v @g p
    , Refl <- ghmap_CONST @k @kw @Type @t p n
    = n

class IsFClass k kw (u :: TyVar kw TT) where
  isF ::
    forall (f :: TT) (r :: LoT k) (a :: Type).
    (Proxy r, Proxy a) ->
    InterpretVar u (r ++. LoT2 f a) :~: f

instance IsFClass Type (TT -> Type -> Type) 'VZ where
  isF _ = Refl

instance IsFClass k kw v => IsFClass (t -> k) (t -> kw) ('VS v) where
  isF (_ :: Proxy r, a) = isF @k @kw @v (tl, a) where
    tl = Proxy @(TailLoT r)

-- **** H Fields

instance
  ( IsFClass k kw v
  , IsConstClass k kw (h :: Atom kw (TT -> Type -> Type))
  , IsConstClass k kw (u :: Atom kw Type)
  , forall (r :: LoT k) (a :: Type). Class_Interpret_NoOccurN2 HFunctor h r a
  ) => GHFunctor_Field k kw 'H (h ':@: 'Var v ':@: u) where
  ghmap_Field
    p@(r :: Proxy r, a :: Proxy a)
    (n :: forall x. f x -> g x)
    | Refl <- isConst @k @kw @h (r, f, a)
    , Refl <- isConst @k @kw @h (r, g, a)
    , Refl <- isF @k @kw @v @f p
    , Refl <- isF @k @kw @v @g p
    , Refl <- ghmap_CONST @k @kw @_ @u p n
    = generalize @(Class_Interpret_NoOccurN2 HFunctor h r a)
        (hmap n)
    where
      f = Proxy @f
      g = Proxy @g

type Class_Interpret_NoOccurN2_ (c :: z -> Constraint) (h :: Atom d z) (r :: LoT k) (a :: Type)
  = c (Interpret (NoOccurN2 h) (r ++. LoT1 a :: LoT (k ++ TT)))
class    Class_Interpret_NoOccurN2_ c h r a => Class_Interpret_NoOccurN2 c h r a
instance Class_Interpret_NoOccurN2_ c h r a => Class_Interpret_NoOccurN2 c h r a

generalize :: forall c r. (c => r) -> (c => r)
generalize x = x

-- **** P Fields

instance
  ( IsConstClass k kw (phi :: Atom kw TT)
  , forall (r :: LoT k) (a :: Type). Class_Interpret_NoOccurN2 Functor phi r a
  , GHFunctor_Field' k kw u
  ) => GHFunctor_Field k kw 'P (phi ':@: u) where
  ghmap_Field
    p@(r :: Proxy r, a :: Proxy a)
    (n :: forall x. f x -> g x)
    | Refl <- isConst @k @kw @phi (r, f, a)
    , Refl <- isConst @k @kw @phi (r, g, a)
    = generalize @(Class_Interpret_NoOccurN2 Functor phi r a)
        (fmap (ghmap_Field' @k @kw @u p n))
    where
      f = Proxy @f
      g = Proxy @g


-- * Example

data CalcF :: TT -> TT where
  DummyConst :: Int -> CalcF f a             -- CONST
  DummyEx    :: f x -> CalcF f ()            -- F (also, using Exists + (:=>:))
  DummyRec   :: CalcF f a -> CalcF f a       -- H
  DummyFunctor :: (Int -> f a) -> CalcF f a  -- P
  DummyUnit     :: CalcF f a                 -- U1
  IntF          :: Int -> CalcF r Int
  StringF       :: String -> CalcF r String
  AddIntF       :: r Int -> r Int -> CalcF r Int  -- (:*:)
  ConcatString  :: r String -> r String -> CalcF r String

$(deriveGenericK ''CalcF)

instance HFunctor CalcF where
  hmap = genericHmap
